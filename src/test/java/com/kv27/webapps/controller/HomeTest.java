package com.kv27.webapps.controller;

import com.kv27.webapps.data.BaseDto;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

public class HomeTest {
    private Home home;

    @Mock
    private Model model;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        home = new Home(model);
    }

    @Test
    public void testHome() throws Exception {
        home.home(model);

        Mockito.verify(model).addAttribute("messageDto", new BaseDto());
    }
}
