package com.kv27.webapps.controller;

import com.kv27.webapps.data.BaseDto;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class Home {
    private Model model;
    static final Logger logger = LoggerFactory.getLogger(Home.class);
    private OAuthService service;
    private Token requestToken;

    public Home() {
    }

    /* TEST ONLY */
    Home(Model model) {
        this.model = model;
    }

    @RequestMapping(value = "home")
    public String home(Model model) {
        model.addAttribute("messageDto", new BaseDto());

        return "home";
    }

    @RequestMapping(value = "ajaxHome")
    @ResponseBody
    public BaseDto ajaxHome() {
        return new BaseDto();
    }
}
