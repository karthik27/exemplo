package com.kv27.webapps.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kv27.webapps.data.LinkedInProfileDto;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

@Controller
public class OAuthController {
    static final Logger logger = LoggerFactory.getLogger(Home.class);

    @RequestMapping(value = "profile/linkedIn/access")
    public String linkedInProfileAccess() throws URIException {
        GetMethod getMethod = new GetMethod("https://www.linkedin.com/uas/oauth2/authorization");
        NameValuePair[] queryParams = {new NameValuePair("response_type", "code"),
                new NameValuePair("client_id", "4kxmhrcwkqjp"),
                new NameValuePair("scope", "r_fullprofile"),
                new NameValuePair("state", "sdssd3r3ersderedfsfd"),
                new NameValuePair("redirect_uri", "http://localhost:8080/exemplo/profile/linkedIn"),
        };
        getMethod.setQueryString(queryParams);

        logger.debug("redirecting to:" + getMethod.getURI());
        return "redirect:" + getMethod.getURI();
    }

    @RequestMapping(value = "profile/linkedIn")
    public String linkedInProfile(HttpServletRequest request, Model model) throws IOException {
        String authorizationCode = request.getParameter("code");
        HttpClient httpClient = new HttpClient();
        PostMethod postMethod = new PostMethod("https://www.linkedin.com/uas/oauth2/accessToken");
        NameValuePair[] queryParams = {
                new NameValuePair("grant_type", "authorization_code"),
                new NameValuePair("code", authorizationCode),
                new NameValuePair("redirect_uri", "http://localhost:8080/exemplo/profile/linkedIn"),
                new NameValuePair("client_id", "4kxmhrcwkqjp"),
                new NameValuePair("client_secret", "6cxP1WsM930eoLOZ"),
        };
        postMethod.setQueryString(queryParams);
        httpClient.executeMethod(postMethod);

        Gson gson = new Gson();
        Type mapOfStringString = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> responseMap = gson.fromJson(postMethod.getResponseBodyAsString(), mapOfStringString);

        GetMethod apiCall = new GetMethod("https://api.linkedin.com/v1/people/~:(first-name,last-name,headline,picture-url,public-profile-url,summary)");
        apiCall.setQueryString(new NameValuePair[]{new NameValuePair("oauth2_access_token", responseMap.get("access_token")), new NameValuePair("format", "json")});
        httpClient.executeMethod(apiCall);

        model.addAttribute("profile", gson.fromJson(apiCall.getResponseBodyAsString(), LinkedInProfileDto.class));
        return "linkedInProfile";
    }

}
